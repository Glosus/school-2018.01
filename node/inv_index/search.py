import os
import pickle

from ...core.document_tokenizer import tokenize_string
from ..logger import LOGGER


class SearchQueryProcessor:
    def __init__(self, config):
        self._config = config
        with open(os.path.join(config["base_dir"], "index/inverted.index", "wb")) as f:
            self._index = pickle.load(f)

        with open(os.path.join(config["base_dir"], "index/inverted.index", "wb")) as f:
            self._str_storage = pickle.load(f)

    def _get_repo_url(self, path: str):
        path.replace(os.path.join(self._config["base_dir"], "repos/"), "")
        folder = path.split("/")[0]
        folder = folder.split(".")
        return "http://github.com/%s/%s" % (folder[0], folder[1])

    def __call__(self, query):
        tokens = tokenize_string(query)
        files = set()
        for token in tokens:
            try:
                ids = self._index[token]
            except KeyError:
                continue
            for index in ids:
                files.add(self._str_storage.get_file_path(index[1]))

        candidates = set()
        for file in files:
            with open(file, "rb") as f:
                try:
                    candidates.add((self._get_repo_url(file), f.read()))
                except FileNotFoundError:
                    LOGGER.warn("Failed to find file:%s but it is index!" % file)
        return list(candidates)
